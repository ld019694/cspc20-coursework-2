#include <iostream>
#include <string>
#include <utility>
#include <vector>
#include <map>
#include <fstream> 
#include <sstream>
#include "Area.h"
#include "Room.h"

void Area::AddRoom(const std::string& name, Room* room) {
    rooms[name] = *room;
};

Room* Area::GetRoom(const std::string& name) {
    if (rooms.find(name) != rooms.end()) {
        return &rooms[name];
    }
    return nullptr;
};

void Area::ConnectRooms(const std::string& roomName, const std::pair<std::string, std::string> Exit) {
 Room* room = GetRoom(roomName);
 Room* conRoom = GetRoom(Exit.first);

    if (room != nullptr && conRoom != nullptr) {
        room->AddExit(Exit.second, conRoom);
    }
    else {
        std::cout << "Could not connect rooms: " << roomName << " and " << Exit.first << std::endl;
    }

};

void Area::LoadMapFromFile(const std::string& filename) {
    std::ifstream file(filename);
    if (!file.is_open()) {
        std::cout << "Could not open file: " << filename << std::endl;
        return;
    }

    std::string line;
    bool connections = false;
    Room r;

    while (std::getline(file, line))
    {
        if (line.empty())
        {
            connections = true;
            continue;
        }

        if (!connections)
        {
            std::stringstream ss(line);
            std::string roomName, roomDesc;
            std::getline(ss, roomName, '|');
            std::getline(ss, roomDesc);
            r = Room(roomName, roomDesc);

            rooms [roomName] = Room(roomName, roomDesc);

            if (roomName == "startRoom") {
                rooms[roomName].AddItem(Item("Health Potions", "Heals the player health by 20 points!"));
            }

            else if (roomName == "oblock") {
                rooms[roomName].AddItem(Item("Health Potions", "Heals the pls{ayer health by 20 points!"));
            }

            else if (roomName == "treasureRoom") {
                rooms[roomName].AddItem(Item("Golden Chalice", "Maybe this is worth something!"));
            }

            else if (roomName == "cauldronRoom") {
                rooms[roomName].AddItem(Item("Mysterious Potion", "I wonder what this potion does!"));
            }

            else if (roomName == "dungeon") {
                rooms[roomName].AddItem(Item("Mysterious Potion", "I wonder what this potion does!"));
            }

        } 
        
        else
        {
            std::stringstream ss(line);
            std::string roomName, conn;
            std::getline(ss, roomName, '|');
            while (std::getline(ss, conn, '|'))
            {
                std::stringstream css(conn);
                std::string connRoom, dir;
                std::getline(css, connRoom, '.');
                std::getline(css, dir);

                std::pair<std::string, std::string> Exit = std::make_pair(connRoom, dir);

                ConnectRooms(roomName, Exit);
            }
        }
        
    
    }
    file.close();

};
