#include <iostream>
#include <string>
#include <vector>
#include <map>
#include "Room.h"

#ifndef AREA_H
#define AREA_H

class Area {

    private:
        std::map<std::string,Room> rooms;
    
    public:
        Area() = default;
        void AddRoom(const std::string& name, Room* room);
        Room* GetRoom(const std::string& name);
        void ConnectRooms(const std::string& roomName, const std::pair<std::string, std::string> Exit);
        void LoadMapFromFile(const std::string& filename);
    
};

#endif //AREA_H