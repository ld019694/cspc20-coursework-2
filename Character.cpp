#include "Character.h"
#include <codecvt>

Character::Character(const std::string& name, int health, int XP) {
    
}

void Character::TakeDamage(int damage) {
    health = health - damage;
    if (health > 0) {
        std::cout << name << " has taken " << damage << " damage. Remaining health: " << health << std::endl;
    }

    else if (health <= 0){
        std::cout << name << " has taken " << damage << " damage. " << name << " has died." << std::endl;
        std::exit(0);
    }
}

void Character::HealDamage(int heal) {
    health = health + heal;
    if (health >= 100) {
        health = 100;
        std::cout << name << " is already at full health" << std::endl;
    }

    else if (health < 100) {
        std::cout << name << " has healed " << heal << " health. Remaining health: " << health << std::endl;
    }
    
}

void Character::AddXP(int XPadd) {
    XP = XP + XPadd;
    std::cout << name << " has gained " << XPadd << " XP. Total XP: " << XP << std::endl;
}



Player::Player(const std::string& name, int health, int XP) : Character(name, health, XP) {
    
}

void Player::SetLocation(Room* NewLocation) {
    location = NewLocation;
}

Room* Player::GetLocation() {
    return location; 
}