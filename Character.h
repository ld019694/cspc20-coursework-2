#include <iostream>
#include <string>
#include <vector>
#include "Item.h"
#include "Room.h"

#ifndef CHARACTER_H
#define CHARACTER_H

class Character {
private:
    std::string name;
    int health;
    int XP;
    std::vector<Item> inventory;

public:
    Character(const std::string& name, int health, int XP);
    void TakeDamage(int damage);
    void HealDamage(int heal);
    void AddXP(int XPadd);
};

class Player : public Character {
private:
    Room* location;

public:
    Player(const std::string& name, int health, int XP);
    void SetLocation(Room* room);
    Room* GetLocation();
};

#endif //CHARACTER_H

