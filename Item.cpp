#include "Item.h"


Item::Item(const std::string& name, const std::string& desc) : name(name), description(desc){}


void Item::Interact() {
    std::cout << "Name: " << name << "Description: " << description << std::endl;
}

std::string Item::GetName() {
   return this->name;
}

std::string Item::GetDescription() {
    return this->description;
}