#include <iostream>
#include <string>

#ifndef ITEM_H
#define ITEM_H

class Item {
private:
    std::string name;
    std::string description;

public:
    Item(const std::string& name, const std::string& desc);
    void Interact();
    std::string GetName();
    std::string GetDescription();
};

#endif //ITEM_H


