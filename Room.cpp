#include "Room.h"
#include "Item.h"

Room::Room(const std::string& name, const std::string& desc) : name(std::move(name)), description(std::move(desc)) {}

void Room::AddItem(const Item& item) {
    items.push_back(item);
}

void Room::RemoveItem(const Item& item) {
    
}

std::vector<Item> Room::GetItems() const {
    return items;
}

std::string Room::GetDescription() const {
    return description;
}

std::string Room::GetName() const {
    return name;
}

void Room::AddExit(const std::string& direction, Room* room) {
    this->exits[direction] = room;
}

Room* Room::GetExit(const std::string& direction) {
    if (exits.count(direction))
        return exits[direction];

    return nullptr;
}    
