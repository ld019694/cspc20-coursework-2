#include <iostream>
#include <string>
#include <map>
#include <vector>
#include "Item.h"

#ifndef ROOM_H
#define ROOM_H

class Room {
private:
    std::string name;
    std::string description;
    std::map<std::string, Room*> exits;
    std::vector<Item> items;

public:
    Room(const std::string& name, const std::string& desc);
    Room() = default;
    void AddItem(const Item& item);
    void RemoveItem(const Item& item);
    std::vector<Item> GetItems() const;
    std::string GetDescription() const;
    std::string GetName() const;
    void AddExit(const std::string& direction, Room* room);
    Room* GetExit(const std::string& direction);
};

#endif //ROOM_H



