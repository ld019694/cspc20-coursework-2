#include <iostream>
#include <string>
#include <vector>
#include <map>
#include "Item.h"
#include "Character.h"
#include "Room.h"
#include "Area.h"


// Include the Room and Item classes (assuming they are defined in separatefiles, which they should be!)
int main() {
    // Create the instance of the Area class

    Area gameWorld;

    //Load the game map from a text file
    gameWorld.LoadMapFromFile("game_map.txt");

    //Create the player
    Player player("Alice", 100, 0); 

    //Set the player's starting room 
    Room* currentRoom = gameWorld.GetRoom("startRoom");
    player.SetLocation(currentRoom);

    // Game loop (basic interaction)
    while (true) {
        std::cout << "Current Location: " << player.GetLocation()->GetDescription() << std::endl;

        std::cout << "Items in the room:" << std::endl;
        for (auto item : player.GetLocation()->GetItems()) {
            std::cout << "- " << item.GetName() << ": " << item.GetDescription() << std::endl;
        }

        std::cout << "Exits available: ";
        std::string roomName = player.GetLocation()->GetName();

        std::vector<std::string> directions = {"north", "south", "east", "west"};
        for (const auto& s : directions)
        {
            auto r = player.GetLocation()->GetExit(s);
            if (r != nullptr)
                std::cout << "Connection " << s << std::endl;
        }

        std::cout << "| ";

        std::cout << "Options: ";
        std::cout << "1. Look around | ";
        std::cout << "2. Interact with an item | ";
        std::cout << "3. Move to another room | ";
        std::cout << "4. Quit" << std::endl;

        int choice;
        std::cin >> choice;
        
        if (choice == 1) {
            // Player looks around (no action required)
            std::cout << "You look around the room." << std::endl;
        } 

        else if (choice == 2) {
            // Player interacts with an item in the room
            std::cout << "Enter the name of the item you want to interact with: ";

            std::string itemName;
            std::getline(std::cin >> std::ws, itemName);

            for (Item& item : player.GetLocation()->GetItems()) {
                if (item.GetName() == itemName) {
                    item.Interact();

                    if (itemName == "Health Potion") {
                        player.HealDamage(20);
                    }

                    else if(itemName == "Golden Chalice") {
                        player.AddXP(100);
                    }

                    else if (itemName == "Mysterious Potion"){
                        int random = rand() % 10 + 1;

                        if (random <= 5) {
                            player.TakeDamage(20);
                        }

                        else if (random > 5) {
                            player.AddXP(50);
                        }
                    }
                }
            }
        } 
        
        else if (choice == 3) {
            // Player moves to another room
            std::cout << "Enter the direction (e.g., north, south): ";
            std::string direction;
            std::cin >> direction;
            
            Room* nextRoom = player.GetLocation()->GetExit(direction);
            if (nextRoom != nullptr) {
                 player.SetLocation(nextRoom);
                 std::cout << "You move to the next room." << std::endl;
                 currentRoom = nextRoom;
            } 
            
            else {
                std::cout << "You can't go that way." << std::endl;
            }
        } 
        
        else if (choice == 4) {
            // Quit the game
            std::cout << "Goodbye!" << std::endl;
            break;
        } 
        
        else {
            std::cout << "Invalid choice. Try again." << std::endl;
        }
    }
 return 0;
}